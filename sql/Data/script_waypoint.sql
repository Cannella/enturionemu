INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 0, 7545.07, -7359.87, 162.354, 4000, 'SAY_START');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 1, 7550.05, -7362.24, 162.236, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 2, 7566.98, -7364.32, 161.739, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 3, 7578.83, -7361.68, 161.739, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 4, 7590.97, -7359.05, 162.258, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 5, 7598.35, -7362.82, 162.257, 4000, 'SAY_PROGRESS_1');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 6, 7605.86, -7380.42, 161.937, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 7, 7605.3, -7387.38, 157.254, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 8, 7606.13, -7393.89, 156.942, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 9, 7615.21, -7400.19, 157.143, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 10, 7618.96, -7402.65, 158.202, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 11, 7636.85, -7401.76, 162.145, 0, 'SAY_PROGRESS_2');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 12, 7637.06, -7404.94, 162.207, 4000, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 13, 7636.91, -7412.59, 162.366, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 14, 7637.61, -7425.59, 162.631, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 15, 7637.82, -7459.06, 163.303, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 16, 7638.86, -7470.9, 162.517, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 17, 7641.4, -7488.22, 157.381, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 18, 7634.46, -7505.45, 154.682, 0, 'SAY_PROGRESS_3');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 19, 7631.91, -7516.95, 153.597, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 20, 7622.23, -7537.04, 151.587, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 21, 7610.92, -7550.67, 149.639, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 22, 7598.23, -7562.55, 145.954, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 23, 7588.51, -7577.76, 148.294, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 24, 7567.34, -7608.46, 146.006, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 25, 7562.55, -7617.42, 148.098, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 26, 7561.51, -7645.06, 151.245, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 27, 7563.34, -7654.65, 151.227, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 28, 7565.53, -7658.3, 151.249, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 30, 7579.12, -7662.21, 151.652, 0, 'quest complete');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 31, 7603.77, -7667, 153.998, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 32, 7603.77, -7667, 153.998, 4000, 'SAY_END_1');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 33, 7603.77, -7667, 153.998, 8000, 'SAY_END_2');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 34, 7603.77, -7667, 153.998, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (16295, 39, 7571.16, -7659.12, 151.245, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (15420, 0, 9294.78, -6682.51, 22.42, 0, 'npc_prospector_anvilward');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (15420, 1, 9298.27, -6667.99, 22.42, 0, 'npc_prospector_anvilward');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (15420, 2, 9309.63, -6658.84, 22.43, 0, 'npc_prospector_anvilward');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (15420, 3, 9304.43, -6649.31, 26.46, 0, 'npc_prospector_anvilward');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (15420, 4, 9298.83, -6648, 28.61, 0, 'npc_prospector_anvilward');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (15420, 5, 9291.06, -6653.46, 31.83, 2500, 'npc_prospector_anvilward');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (15420, 6, 9289.08, -6660.17, 31.85, 5000, 'npc_prospector_anvilward');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (15420, 7, 9291.06, -6653.46, 31.83, 0, 'npc_prospector_anvilward');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 1, 781.513, 657.99, 466.821, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 2, 759.005, 665.142, 462.541, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 3, 732.937, 657.164, 452.678, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 4, 717.491, 646.009, 440.137, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 5, 707.57, 628.978, 431.129, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 6, 705.164, 603.628, 422.957, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 7, 716.351, 588.49, 420.802, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 8, 741.703, 580.168, 420.523, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 9, 761.634, 586.383, 422.206, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 10, 775.983, 601.992, 423.606, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 11, 769.051, 624.686, 420.035, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 12, 756.582, 631.692, 412.53, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35491, 13, 744.841, 634.505, 411.575, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35492, 1, 741.067, 634.472, 411.569, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35492, 2, 735.726, 639.247, 414.726, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35492, 3, 730.187, 653.251, 418.913, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35492, 4, 734.518, 666.071, 426.259, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35492, 5, 739.638, 675.339, 438.227, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35492, 6, 741.834, 698.797, 456.986, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35492, 7, 734.647, 711.085, 467.165, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35492, 8, 715.388, 723.821, 470.334, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (35492, 9, 687.179, 730.141, 470.569, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (28912, 0, 1653.52, -6038.37, 127.585, 1000, 'Jump off');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (28912, 1, 1653.98, -6034.61, 127.585, 5000, 'To Box');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (28912, 2, 1653.85, -6034.73, 127.585, 0, 'Equip');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (28912, 3, 1652.3, -6035.67, 127.585, 1000, 'Recover');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (28912, 4, 1639.76, -6046.34, 127.948, 0, 'Escape');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (28912, 5, 1640.96, -6028.12, 134.74, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (28912, 6, 1625.81, -6029.2, 134.74, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (28912, 7, 1626.84, -6015.08, 134.74, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (28912, 8, 1649.15, -6016.98, 133.24, 0, '');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (28912, 9, 1653.06, -5974.84, 132.652, 5000, 'Mount');
INSERT INTO `script_waypoint` (`entry`, `pointid`, `location_x`, `location_y`, `location_z`, `waittime`, `point_comment`) VALUES (28912, 10, 1654.75, -5926.42, 121.191, 0, 'Disappear');
