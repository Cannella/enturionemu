INSERT INTO `outdoorpvp_template` (`TypeId`, `ScriptName`, `comment`) VALUES (1, 'outdoorpvp_hp', 'Hellfire Peninsula');
INSERT INTO `outdoorpvp_template` (`TypeId`, `ScriptName`, `comment`) VALUES (2, 'outdoorpvp_na', 'Nagrand');
INSERT INTO `outdoorpvp_template` (`TypeId`, `ScriptName`, `comment`) VALUES (3, 'outdoorpvp_tf', 'Terokkar Forest');
INSERT INTO `outdoorpvp_template` (`TypeId`, `ScriptName`, `comment`) VALUES (4, 'outdoorpvp_zm', 'Zangarmarsh');
INSERT INTO `outdoorpvp_template` (`TypeId`, `ScriptName`, `comment`) VALUES (5, 'outdoorpvp_si', 'Silithus');
INSERT INTO `outdoorpvp_template` (`TypeId`, `ScriptName`, `comment`) VALUES (6, 'outdoorpvp_ep', 'Eastern Plaguelands');
INSERT INTO `outdoorpvp_template` (`TypeId`, `ScriptName`, `comment`) VALUES (7, 'outdoorpvp_gh', 'Grizzly Hills');
